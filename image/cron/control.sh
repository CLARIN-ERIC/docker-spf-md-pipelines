#!/bin/bash

env_path='/srv/SPF/'
pyff_config_directory_path="${env_path:?}/pyFF_config/"
pyFF_config_version="${PYFF_CONF_VERSION:-master}"
pyFF_config_url="https://codeload.github.com/clarin-eric/pyFF_config/tar.gz/${pyFF_config_version}"
spf_sp_md_release="latest"
spf_sp_md_branch="master"
spf_sp_md_api_url="https://api.github.com/repos/clarin-eric/SPF-SPs-metadata/releases/${spf_sp_md_release}"
centre_registry_sps_url="https://centres.clarin.eu/api/model/SAMLServiceProvider"

# bugfix make pyff run directory writeable by all group users
chmod -R g+w /var/run/pyff

_curl() {
    'curl' --verbose --silent --fail --show-error --ipv4 \
      --connect-timeout 5 --max-time 300 --retry 5 --retry-delay 0 \
      --retry-max-time 60 --location --time-cond "$1" --output "$1" "$2"
    return $?
}

sign() {
    printf '%s' "Signing file: '${1}' ... "
    xmlsign_parameters=(--loglevel INFO --key /home/keys/SPF_signing_priv.pem --cert /home/keys/SPF_signing_pub.crt --reference @ID)
    'xmlsign' "${xmlsign_parameters[@]}" -o "${1}" "${1}" && echo "Done!"
    return $?
}

verify() {
    printf '%s\n' "Verifying file: '${1}' ... "
    xmlverify_parameters=(--verify --id-attr:ID urn:oasis:names:tc:SAML:2.0:metadata:EntitiesDescriptor --trusted-pem /home/keys/SPF_signing_pub.crt)
    'xmlsec1' "${xmlverify_parameters[@]}" "${1}"
    return $?
}

run() {
    failed_steps=0
    if ! fetch_config; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_fetch_md; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! build_prod_config; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_run job_a; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_run job_b; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_run job_c; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_run job_e; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_run job_f; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_sign; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_verify_signatures; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    if ! pyff_publish; then failed_steps=$((failed_steps + 1)); fi
    echo "Number of steps with errors in \`control.sh\` sequence: " $failed_steps
    return $failed_steps
}

build_prod_config() {
    echo "Fetching list of production SPs from CLARIN Centre Registry..."
    SPs=$(curl ${centre_registry_sps_url} 2> /dev/null | \
       jq -r ".[] | select(.fields.production_status == true) | .fields.entity_id")
    echo "Building pyFF configuration for production SPs..."
    cp "productionSPs_template.fd.tpl" "job_b.fd"
    echo "The following SPs will be exported to the production feed:"
    for sp in ${SPs}
    do 
        echo "${sp}"
        sed -i "/\- select\:/a \ \ \ - $sp" "job_b.fd"
    done
    return 0
}

fetch_config() {
    error=() 
    (_curl "${pyff_config_directory_path}/${pyFF_config_version}".tar.gz "${pyFF_config_url}" &&
    tar xvf "${pyFF_config_version}".tar.gz &&
    # keep the old output folder
    rm -rf pyFF_config-"${pyFF_config_version}"/output &&
    find pyFF_config-"${pyFF_config_version}"/ -name ".*" -type f -delete &&
    cp -rp pyFF_config-"${pyFF_config_version}"/* . &&
    rm -rf pyFF_config-"${pyFF_config_version}" "${pyFF_config_version}".tar.gz) ||
    (error+=("fetch_config -> exit status: $?") && printf '%s \n' "WARNING: Error updating SPF configuration from: ${pyFF_config_url}." && 
        print_error_array && return 1)
    printf '%s \n' "SPF configuration updated from: ${pyFF_config_url}."
    return 0
}

pyff_fetch_md() {
    error=()
    output_dir_path="${pyff_config_directory_path:?}/output/"
    id_feds_target_dir_path="$(readlink -f -- "${output_dir_path:?}/id_feds")"
    temp_dir_path="$(readlink -f -- "$(mktemp -d -t 'pyff_fetch_md.XXXXXX')")" &&
    rsync --remove-source-files -auv "${id_feds_target_dir_path:?}/" "${temp_dir_path}" &&
    
    if [ ! -d "${output_dir_path:?}/sps-metadata" ]; then 
        mkdir "${output_dir_path:?}/sps-metadata"
    fi &&

    printf '%s \n' "Downloading production SAML metadata of CLARIN SPs from Git into ${output_dir_path} ..." &&
    latest_release=$(curl "${spf_sp_md_api_url}" 2> /dev/null | jq -r .tarball_url) &&
    _curl "${output_dir_path:?}/${spf_sp_md_release}.tar.gz" "${latest_release}" &&
    rm -rf "${output_dir_path:?}/sps-metadata/production" "${output_dir_path:?}/${spf_sp_md_release}/" && mkdir "${output_dir_path:?}/${spf_sp_md_release}" &&
    tar xvf "${output_dir_path:?}/${spf_sp_md_release}.tar.gz" --strip-components 1 -C "${output_dir_path:?}/${spf_sp_md_release}" &&
    mv "${output_dir_path:?}/${spf_sp_md_release}/metadata" "${output_dir_path:?}/sps-metadata/production" && rm -rf "${output_dir_path:?}/${spf_sp_md_release}" "${output_dir_path:?}/${spf_sp_md_release}.tar.gz" &&
    for file in "${output_dir_path:?}"/sps-metadata/production/*.xml; do mv "$file" "${output_dir_path:?}"/sps-metadata/production/"$(basename "$file" | tr -cd '.A-Za-z0-9_-')"; done &&
    printf '%s \n' "Downloaded production SAML metadata of CLARIN SPs from ${latest_release}" ||
    error+=("CLARIN SPF SAML production metadata export from github repository -> exit status: $?")

    printf '%s \n' "Downloading SPs staging SAML metadata of CLARIN SPs from Git into ${output_dir_path} ..." &&
    repo_head="https://codeload.github.com/clarin-eric/SPF-SPs-metadata/tar.gz/${spf_sp_md_branch}"
    _curl "${output_dir_path:?}/${spf_sp_md_branch}.tar.gz" "${repo_head}" &&
    rm -rf "${output_dir_path:?}/sps-metadata/staging" "${output_dir_path:?}/${spf_sp_md_branch}/" && mkdir "${output_dir_path:?}/${spf_sp_md_branch}" &&
    tar xvf "${output_dir_path:?}/${spf_sp_md_branch}.tar.gz" --strip-components 1 -C "${output_dir_path:?}/${spf_sp_md_branch}" &&
    mv "${output_dir_path:?}/${spf_sp_md_branch}/metadata" "${output_dir_path:?}/sps-metadata/staging" && rm -rf "${output_dir_path:?}/${spf_sp_md_branch}" "${output_dir_path:?}/${spf_sp_md_branch}.tar.gz" &&
    for file in "${output_dir_path:?}"/sps-metadata/staging/*.xml; do mv "$file" "${output_dir_path:?}"/sps-metadata/staging/"$(basename "$file" | tr -cd '.A-Za-z0-9_-')"; done &&
    printf '%s \n' "Downloaded SPs staging SAML metadata of CLARIN SPs from ${repo_head}" ||
    error+=("CLARIN SPF SAML staging metadata export from github repository -> exit status: $?")

    printf '%s \n' "Updating SAML metadata batches about IdPs from identity federations into '${temp_dir_path:?}' unless already up-to-date in '${id_feds_target_dir_path:?}' ..."
    _curl "${temp_dir_path}/aconet.xml" 'https://eduid.at/md/aconet-registered.xml' ||
    error+=("pyff_fetch_md: ACOnet -> exit status: $?")
    _curl "${temp_dir_path}/surfconext.xml" 'https://metadata.surfconext.nl/signed/2023/sp/https%253A%252F%252Fsp.catalog.clarin.eu' ||
    error+=("pyff_fetch_md: SURFconext -> exit status: $?")
    _curl "${temp_dir_path}/dfn-aai-basic.xml" 'https://www.aai.dfn.de/fileadmin/metadata/dfn-aai-basic-metadata.xml' ||
    error+=("pyff_fetch_md: DFN-AAI-Basic & Advanced -> exit status: $?")
    _curl "${temp_dir_path}/belnet.xml" 'https://federation.belnet.be/federation-metadata.xml' ||
    error+=("pyff_fetch_md: Belnet -> exit status: $?")
    _curl "${temp_dir_path}/eduidcz.xml" 'https://metadata.eduid.cz/entities/eduid+idp' ||
    error+=("pyff_fetch_md: eduID.cz -> exit status: $?")
    _curl "${temp_dir_path}/swamid_edugain.xml" 'https://mds.swamid.se/md/swamid-edugain-1.0.xml' ||
    error+=("pyff_fetch_md: SWAMID eduGAIN -> exit status: $?")
    _curl "${temp_dir_path}/arnesaai_edugain.xml" 'https://ds.aai.arnes.si/metadata/arnesaai2edugain.signed.xml' ||
    error+=("pyff_fetch_md: ArnesAAI eduGAIN -> exit status: $?")
    _curl "${temp_dir_path}/rcts.xml" 'https://registry.rctsaai.pt/rr/signedmetadata/federation/rctsaai/metadata.xml' ||
    error+=("pyff_fetch_md: RCTS -> exit status: $?")
    _curl "${temp_dir_path}/edugain.xml" 'https://mds.edugain.org/edugain-v2.xml' ||
    error+=("pyff_fetch_md: eduGAIN -> exit status: $?")

    rsync --remove-source-files -auv "${temp_dir_path}/" "${id_feds_target_dir_path}"

    rm -rf "${temp_dir_path}"

    if ((${#error[@]})); then
        printf '%s \n' "WARNING: Errors while updating SAML metadata batches about IdPs in '${id_feds_target_dir_path}'."
        print_error_array && return 1
    fi
    printf '%s \n' "SAML metadata batches about IdPs up-to-date in '${id_feds_target_dir_path}'."
    return 0
}

pyff_run() {
    printf '%s\n' "Running PyFF job ${1}"
    pyff --loglevel=INFO "${1}".fd
    fd_exit_code=$?
    return $fd_exit_code
}

pyff_sign() {
    error=()

    sign 'output/md_about_spf_sps.xml' ||
    error+=("pyff_sign: md_about_spf_sps -> exit status: $?")
    sign 'output/prod_md_about_clarin_erics_idp.xml' ||
    error+=("pyff_sign: prod_md_about_clarin_erics_idp -> exit status: $?")
    sign 'output/prod_md_about_spf_idps.xml' ||
    error+=("pyff_sign: prod_md_about_spf_idps -> exit status: $?")
    sign 'output/prod_md_about_spf_sps.xml' ||
    error+=("pyff_sign: prod_md_about_spf_sps -> exit status: $?")
    sign 'output/prod_md_about_edugain_idps.xml' ||
    error+=("pyff_sign: prod_md_about_edugain_idps -> exit status: $?")

    if ((${#error[@]})); then
        echo 'WARNING: Errors signing SAML metadata batches ... ' ;
        print_error_array && return 1
    fi
    echo 'Signed SAML metadata batches ... ' ;
    return 0
}

pyff_verify_signatures() {
    error=()

    verify 'output/md_about_spf_sps.xml' ||
    error+=("pyff_verify_signatures: md_about_spf_sps -> exit status: $?")
    verify 'output/prod_md_about_clarin_erics_idp.xml' ||
    error+=("pyff_verify_signatures: prod_md_about_clarin_erics_idp -> exit status: $?")
    verify 'output/prod_md_about_spf_idps.xml' ||
    error+=("pyff_verify_signatures: prod_md_about_spf_idps -> exit status: $?")
    verify 'output/prod_md_about_spf_sps.xml' ||
    error+=("pyff_verify_signatures: prod_md_about_spf_sps -> exit status: $?")
    verify 'output/prod_md_about_edugain_idps.xml' ||
    error+=("pyff_verify_signatures: prod_md_about_edugain_idps -> exit status: $?")

    if ((${#error[@]})); then
        echo 'WARNING: Errors verifying SAML metadata batch signatures ... '
        print_error_array && return 1
    fi
    echo 'Verified SAML metadata batch signatures ... '
    return 0
}

pyff_publish() {
    error=()

    chgrp -Rv nginx 'output/' &&
    find output/ -type d -exec chmod -v 'u=rwx,g=r,o=' {} \; &&
    find output/ -type f -exec chmod -v 'u=rw,g=r,o=' {} \; &&
    mv -f 'output/md_about_spf_sps.xml' \
      'output/prod_md_about_clarin_erics_idp.xml' \
      'output/prod_md_about_spf_idps.xml' 'output/prod_md_about_spf_sps.xml' 'output/prod_md_about_edugain_idps.xml' \
      '/srv/www/infra.clarin.eu/aai/' || error+=("pyff_publish: init -> exit status: $?")

    if ((${#error[@]})); then
        echo 'WARNING: Errors publishing CLARIN SPF SAML metadata ... '
        print_error_array && return 1
    fi
    echo 'Published CLARIN SPF SAML metadata ... '
    return 0
}

print_error_array() {
    echo 'One or more ERRORS occurred:'
    printf '%s\n' "${error[@]}"
    unset error
    # TODO: distinguish fatal and nonfatal failure
}
