#!/usr/bin/env python3
# Compare entities in CLARIN Centre API with eduGAIN 'clarin-member' Entity Category. -- maintainer: andre@clarin.eu / original code: peter@aco.net
import os
import logging
import argparse
import requests
from lxml import etree

# Logging
parser = argparse.ArgumentParser()
parser.add_argument("--log-level", choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help="Set the logging level", default="INFO")
args = parser.parse_args()
logging.basicConfig(level=getattr(logging, args.log_level), format='%(asctime)s [%(levelname)s]: %(message)s')

# Configurables
clarin_api_url = 'https://centres.clarin.eu/api/model/SAMLServiceProvider'
edugain_md_url = 'https://mds.edugain.org/edugain-v2.xml'
report_file = '/srv/www/infra.clarin.eu/aai/sps_missing_clarin-member_in_edugain.html'

NS = {
  "md": "urn:oasis:names:tc:SAML:2.0:metadata",
  "mdrpi": "urn:oasis:names:tc:SAML:metadata:rpi",
  "mdattr": "urn:oasis:names:tc:SAML:metadata:attribute",
  "saml": "urn:oasis:names:tc:SAML:2.0:assertion",
}

def clarin_api(url):
    logging.info("Fecthing CLARIN SPs from the CLARIN Centre Registry API: {}".format(clarin_api_url))
    entities = set()
    r = requests.get(url, timeout=30).json()
    for e in r:
        try:
            if e['fields']['production_status']:
                entities.add(e['fields']['entity_id'])
        except KeyError:
            pass
    return entities

def process_edugain(url):
    logging.info("Fecthing full eduGAIN metadata from: {}".format(edugain_md_url))
    r = requests.get(url, timeout=30).content
    tree = etree.fromstring(r)
    return tree

def edugain_md(tree):
    logging.info("Selecting 'clarin-member' SPs from eduGAIN...")
    entities = set()
    for e in tree.xpath("//md:EntityDescriptor[md:Extensions/mdattr:EntityAttributes/saml:Attribute[\
                        @Name='http://macedir.org/entity-category']/saml:AttributeValue[text() =\
                        'http://clarin.eu/category/clarin-member']]", namespaces=NS):
        entities.add(e.get('entityID'))
    return entities

def registrar(entity, tree):
    logging.debug("Getting registration authority for SP: {}".format(entity))
    reg = tree.xpath("//md:EntityDescriptor[@entityID = '{}']\
                     /md:Extensions/mdrpi:RegistrationInfo/@registrationAuthority".format(entity),
                     namespaces=NS)[0]
    return reg

et = process_edugain(edugain_md_url)
api_entities = clarin_api(clarin_api_url)
edg_entities = edugain_md(et)

missing = api_entities.difference(edg_entities)
unexpected = edg_entities.difference(api_entities)

try:
    os.remove(report_file)
except FileNotFoundError:
    logging.warning("The file {} does not exist yet".format(report_file))

f = open(report_file, "a")
print("<!DOCTYPE html>", file=f)
print("<html lang=\"en\">", file=f)
print("<head>", file=f)
print("<meta charset=\"utf-8\"/>", file=f)
print("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">", file=f)
print("<title>clarin-member Entity Category report</title>", file=f)
print("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">", file=f)
print("</head>", file=f)
print("<body>", file=f)
print("<div class=\"container-fluid\">", file=f)

missing_ec = set()
missing_et = set()
for eid in missing:
    if not et.xpath("//md:EntityDescriptor[@entityID = '{}']".format(eid),
        namespaces=NS):
        missing_et.add(eid)
    else:
        missing_ec.add(eid)

if missing_et:
    print("<h4>entityIDs in the CLARIN Centre Registry but missing in eduGAIN:</h4><ul>", file=f)
    for eid in sorted(missing_et):
        logging.info("Found SP missing in eduGAIN but present in the CLARIN Centre Registry: {}".format(eid))
        print("", file=f)
        print("<li>{}</li>".format(eid), file=f)
    print("</ul>", file=f)

if missing_ec:
    print("<h4>entityIDs in the CLARIN Centre Registry but missing the \"clarin-member\" category in eduGAIN:</h4><ul>", file=f)
    for eid in sorted(missing_ec):
        logging.info("Found SP missing the \"clarin-member\" entity category in eduGAIN but present in the CLARIN Centre Registry: {}".format(eid))
        print("", file=f)
        print("<li>{} (Registered by: {})</li>".format(eid, registrar(eid, et)), file=f)
    print("</ul>", file=f)

if unexpected:
    if missing: print("", file=f)
    print("<h4>entityIDs in eduGAIN carrying the \"clarin-member\" entity category but missing from the CLARIN Centre Registry:</h4><ul>", file=f)
    for eid in sorted(unexpected):
        logging.info("Found SP in eduGAIN carrying the \"clarin-member\" entity category but missing from the CLARIN Centre Registry: {}".format(eid))
        print("<li>{} (Registered by: {})</li>".format(eid, registrar(eid, et)), file=f)

    print("</ul>", file=f)
print("</div></body>", file=f)

f.close()
