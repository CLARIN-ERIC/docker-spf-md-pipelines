#!/bin/bash

# Save cronjob exist status
echo "${WEXITSTATUS}" > "${PWD}/exitstatus"

[ "${WEXITSTATUS}" = '0' ] && exit 0 || exit 1 ;