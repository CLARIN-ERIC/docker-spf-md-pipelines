#!/bin/bash
set -e

initialize() {

# Needed to have environment variables available in the crontab
echo PYFF_CONF_VERSION="${PYFF_CONF_VERSION}" >> /etc/cron_env

if [ "$(ls -A "${HOST_KEYS_DIR}")" ]; then
    #
    # Initialize SAML keys/certificate configuration
    #
    echo "Copying SAML certificate and key from host mount..."
    rm -rf "${KEYS_DIR:-?}"/*
    cp "${HOST_KEYS_DIR}"/* "${KEYS_DIR}"/
    chown -Rv "${USER}" "${KEYS_DIR}"
    chmod -v 400 "${KEYS_DIR:-?}"/*
    chmod -v 700 "${KEYS_DIR}"/
else
    echo "SAML certificate and key not supplied in ${HOST_KEYS_DIR}. Generating local certificates"
    # for development purposes we're just going to use openssl certificates to sign the metadata
    cd /tmp || exit 1
    openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
    openssl rsa -passin pass:x -in server.pass.key -out server.key
    rm -rf server.pass.key
    openssl req -new -key server.key -out server.csr -subj "/C=NL/ST=Gelderland/L=Nijmegen/O=CLARIN ERIC/OU=IT Department/CN=example.com"
    openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
    mv server.crt "${KEYS_DIR}"/SPF_signing_pub.crt
    mv server.key "${KEYS_DIR}"/SPF_signing_priv.pem
    chmod 0600 "${KEYS_DIR}"/SPF_signing_priv.pem
    chown -Rv "${USER}" "${KEYS_DIR}"
fi

if [ ! "$(ls -A "${AAI_WEBFILES_DIR}")" ]; then
    echo "Metadata directory ${AAI_WEBFILES_DIR} is empty. Running cron jobs"
    crontab -u "${USER}" -l | grep -v '^#' | cut -f 6- -d ' ' | su - "${USER}"

    if pgrep "check_test.sh"; then
        echo "Crontab run failed in test mode. Exiting with error code = sum(cronjobX_exitcode)."

        read -r -a exit_files_array < <( for d in "${CRON_STATE_DIR}"/* ; do if [ -f "${d%}/exitstatus" ]; then  echo -n "${d%}/exitstatus ";fi;done; echo "" )
        exit "$(paste -d+ "${exit_files_array[@]}" | bc)"
    fi
fi
}

initialize

